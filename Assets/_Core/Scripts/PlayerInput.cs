using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Node HandleMouseInput()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            return ChooseNode();
        }
        else
        {
            return null;
        }
    }

    private Node ChooseNode()
    {
        Vector2 raycastPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit2D = Physics2D.Raycast(raycastPosition, Vector2.zero);
        Node chosenNode = null;
        if (hit2D.collider != null)
        {
            chosenNode = hit2D.collider.GetComponent<Node>();
        }
        if (chosenNode != null)
        {
            return chosenNode;
        }
        else
        {
            return null;
        }
    }
}
