using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{

    [SerializeField] private GameManager _gameManager;
    [SerializeField] private GameObject _pausePanel;
    [SerializeField] private GameObject _winPanel;

    void Update()
    {
        CheckInput();
    }

    private void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePausePanel();
        }
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void TogglePausePanel()
    {
        TogglePanel(_pausePanel);
        ToggleGamePause();
    }

    public void ToggleWinPanel()
    {
        TogglePanel(_winPanel);
    }

    private void TogglePanel(GameObject panel)
    {
        panel.SetActive(!panel.activeInHierarchy);
        TogglePlayerInput();
    }

    private void ToggleGamePause()
    {
        if (Time.timeScale == 1f)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    private void TogglePlayerInput()
    {
        _gameManager.isWaitingForPlayerInput = !_gameManager.isWaitingForPlayerInput;
    }




}
