using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    [SerializeField] private LevelConstructor _levelConstructor;
    [SerializeField] private PlayerInput _playerInput;
    [SerializeField] private UIController _uiController;
    [SerializeField] private SoundsNamesContainer _sounds;
    [SerializeField] private float _chipMoveSpeed = 10f;
    [SerializeField] private float _highlightToggleDuration = 0.5f;

    public bool isWaitingForPlayerInput = true;

    private List<Node> _nodes = new List<Node>();
    private List<Chip> _chips = new List<Chip>();

    private List<int> _checkedNodesIndexes = new List<int>();

    private Node _chosenNode1;
    private Node _chosenNode2;
    private Chip _chosenChip;

    private void Start()
    {
        _levelConstructor.ConstructLevel();
        InitNodes();
        InitChips();
    }

    private void Update()
    {
        HandlePlayerInput();
    }

    // Game Loop:
    // 
    // + INPUT: Choose node with chip
    // + FindPaths for this node
    // + Highlight reachable nodes 
    // + INPUT: Choose reachable node
    // + Remove highlight
    // + Clear Path in every node from _checkedNodesIndexes list
    // + Clear _checkedNodesIndexes list
    // + Move chip to this node
    // + Check every chip if it is on its finish position
    // 
    // ready to repeat


    private void InitChips()
    {
        _chips = _levelConstructor.Chips;
    }


    private void InitNodes()
    {
        _nodes = _levelConstructor.Nodes;
    }


    private void HandlePlayerInput()
    {
        if (isWaitingForPlayerInput == true)
        {
            if (_chosenNode1 == null)
            {
                PlayerChoosingFirstNode();
            }
            else if (_chosenNode2 == null)
            {
                PlayerChoosingSecondNode();
            }
        }
    }


    private void PlayerChoosingFirstNode()
    {
        _chosenNode1 = _playerInput.HandleMouseInput();
        if (_chosenNode1 != null)
        {
            if (_chosenNode1.chip == null)
            {
                _chosenNode1 = null;
            }
            else
            {
                HandleFirstNodeChosenState();
            }
        }
    }


    private void PlayerChoosingSecondNode()
    {
        _chosenNode2 = _playerInput.HandleMouseInput();
        if (_chosenNode2 != null)
        {
            if (_chosenNode2.path.Count == 0)
            {
                _chosenNode2 = null;
            }
            else if (_chosenNode2 == _chosenNode1)
            {
                AudioManager.Instance.Play(_sounds.Click2);
                ResetAllChoices();
            }
            else
            {
                HandleSecondNodeChosenState();
            }
        }
    }


    private void HandleFirstNodeChosenState()
    {
        _chosenChip = _chosenNode1.chip;
        isWaitingForPlayerInput = false;
        FindPaths(_chosenNode1.index, new List<int>());
        ToggleNodesHighlight(_checkedNodesIndexes, true);
        ToggleChipHighlight(_chosenChip, true);
        AudioManager.Instance.Play(_sounds.Click1);
        isWaitingForPlayerInput = true;
    }


    private void HandleSecondNodeChosenState()
    {
        isWaitingForPlayerInput = false;
        AudioManager.Instance.Play(_sounds.Click2);
        MoveChip(_chosenNode1, _chosenNode2, _chosenChip);
        // + callback
    }


    private void ResetAllChoices()
    {
        ToggleNodesHighlight(_checkedNodesIndexes, false);
        ToggleChipHighlight(_chosenChip, false);
        ClearChosenNodesAndChip();
        ClearPaths();
        _checkedNodesIndexes.Clear();
        isWaitingForPlayerInput = true;
    }


    private void MoveChip(Node startNode, Node targetNode, Chip chip)
    {
        var moveSequence = DOTween.Sequence();
        for (int i = 1; i < targetNode.path.Count; i++)
        {
            Node currentNode = _nodes[targetNode.path[i - 1]];
            Node nextNode = _nodes[targetNode.path[i]];
            float distance = (nextNode.transform.position - currentNode.transform.position).magnitude;
            float timeToMove = distance / _chipMoveSpeed;
            moveSequence.Append(chip.transform.DOMove(nextNode.transform.position, timeToMove).SetEase(Ease.Linear));
            moveSequence.OnComplete(() => ChipFinishedMovingCallback(startNode, targetNode, chip));
        }
    }


    private void ChipFinishedMovingCallback(Node startNode, Node targetNode, Chip chip)
    {
        UpdateChipInfo(targetNode, chip);
        UpdateNodesInfo(startNode, targetNode, chip);
        ResetAllChoices();
        if (CheckChipsPositions() == true)
        {
            AudioManager.Instance.Play(_sounds.Win);
            _uiController.ToggleWinPanel();
        }
    }


    private void UpdateNodesInfo(Node startNode, Node targetNode, Chip chip)
    {
        startNode.chip = null;
        targetNode.chip = chip;
    }


    private void UpdateChipInfo(Node targetNode, Chip chip)
    {
        chip.currentNodeIndex = targetNode.index;
    }


    private void ClearChosenNodesAndChip()
    {
        _chosenNode1 = null;
        _chosenNode2 = null;
        _chosenChip = null;
    }


    private bool CheckChipsPositions()
    {
        bool isEveryChipAtFinalPosition = true;
        foreach (Chip chip in _chips)
        {
            Chip chipInfo = chip;
            if (chipInfo.currentNodeIndex != chipInfo.finishNodeIndex)
            {
                isEveryChipAtFinalPosition = false;
            }
        }
        return isEveryChipAtFinalPosition;
    }


    private void ClearPaths()
    {
        for (int i = 0; i < _checkedNodesIndexes.Count; i++)
        {
            _nodes[_checkedNodesIndexes[i]].path.Clear();
        }
    }


    private void ToggleNodesHighlight(List<int> nodesIndexes, bool highlightState)
    {
        foreach (int nodeIndex in nodesIndexes)
        {
            ToggleHighlight(_nodes[nodeIndex].highlighter, highlightState);
        }
    }


    private void ToggleChipHighlight(Chip chip, bool highlightState)
    {
        ToggleHighlight(chip.highlighter, highlightState);
    }


    private void ToggleHighlight(GameObject gameObject, bool highlightState)
    {
        Color color = gameObject.GetComponent<SpriteRenderer>().color;
        if (highlightState == true)
        {
            color = new Color(color.r, color.g, color.b, 1f);
        }
        else
        {
            color = new Color(color.r, color.g, color.b, 0f);
        }
        gameObject.GetComponent<SpriteRenderer>().DOColor(color, _highlightToggleDuration);
    }


    private void FindPaths(int currentNodeIndex, List<int> passedPath)
    {
        Node currentNode = _nodes[currentNodeIndex];
        if (_checkedNodesIndexes.Contains(currentNodeIndex) == false && (currentNode.chip == null || currentNodeIndex == _chosenNode1.index))
        {
            _checkedNodesIndexes.Add(currentNodeIndex);
            currentNode.path = new List<int>(passedPath);
            currentNode.path.Add(currentNodeIndex);
            foreach (Node node in currentNode.connectedNodes)
            {
                FindPaths(node.index, currentNode.path);
            }
        }
    }
}
