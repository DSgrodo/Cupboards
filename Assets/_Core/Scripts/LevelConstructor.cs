using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConstructor : MonoBehaviour
{
    [SerializeField] private DataInputFilesContainer _dataInputFilesContainer;
    [SerializeField] private PlayerPrefsKeysContainer _playerPrefsKeys;
    [SerializeField] private Camera _camera;
    [SerializeField] private float _cameraViewBuffer = 100f;

    [SerializeField] private GameObject _tutorialCanvas;

    [SerializeField] private GameObject _nodePrefab;
    [SerializeField] private GameObject _chipPrefab;
    [SerializeField] private GameObject _linePrefab;

    [HideInInspector] public List<Node> Nodes = new List<Node>();
    [HideInInspector] public List<Chip> Chips = new List<Chip>();

    private List<Connection> Connections = new List<Connection>();

    private readonly string[] linesSeparator = { "\r\n", "\r", "\n" };
    private readonly char[] valuesSeparators = { ',' };

    private TextAsset _textAsset;
    private string _text;
    private string[] _linesInText;

    private int _connectionsNumber;
    private int _nodesNumber;
    private int _chipsNumber;

    private int _currentLine = 0;

    private List<Color> _contrastColors = new List<Color>();

    public void ConstructLevel()
    {
        if (ConstructingTutorial())
        {
            SetUpTutorialElements();
        }

        InitInputDataText();
        SplitTextIntoLines(_text);

        ReadChipsNumber();
        ReadNodesNumber();
        SpawnChips();
        SpawnNodes();

        HandleNodesPositionsInfo();
        HandleChipsStartNodesInfo();
        HandleChipsFinishNodesInfo();

        ReadConnectionsNumber();
        CreateConnectionsList();

        HandleConnectionsNodesInfo();

        AddConnectedNodesInfoToNodes();

        DrawConnections();

        GenerateContrastColors();
        ColorizeChipsAndNodes();
        SetUpCamera();
    }

    private void InitInputDataText()
    {
        _textAsset = _dataInputFilesContainer.DataInputFiles[PlayerPrefs.GetInt(_playerPrefsKeys.InputDataFileIndex)];
        _text = _textAsset.text;
    }

    private void SetUpTutorialElements()
    {
        _cameraViewBuffer *= 2;
        _tutorialCanvas.SetActive(true);
    }

    private bool ConstructingTutorial()
    {
        return PlayerPrefs.GetInt(_playerPrefsKeys.InputDataFileIndex) == 0;
    }

    private void SetUpCamera()
    {
        var bounds = new Bounds(Nodes[0].transform.position, Vector3.zero);

        for (int i = 0; i < _nodesNumber; i++)
        {
            bounds.Encapsulate(Nodes[i].transform.position);
        }

        bounds.Expand(_cameraViewBuffer);

        var vertical = bounds.size.y;
        var horizontal = bounds.size.x * _camera.pixelHeight / _camera.pixelWidth;

        var size = Mathf.Max(horizontal, vertical) * 0.5f;
        var center = bounds.center + new Vector3(0, 0, -10);

        _camera.transform.SetPositionAndRotation(center, Quaternion.identity);
        _camera.orthographicSize = size;
    }


    private void GenerateContrastColors()
    {
        for (float i = 0f; i <= 1f; i += 0.1f)
        {
            _contrastColors.Add(UnityEngine.Random.ColorHSV(i, i, 1f, 1f, 0.5f, 1f));
        }
    }


    private void ColorizeChipsAndNodes()
    {
        Color color;
        foreach (Chip chip in Chips)
        {
            if (_contrastColors.Count > 0)
            {
                int colorIndex = UnityEngine.Random.Range(0, _contrastColors.Count - 1);
                color = _contrastColors[colorIndex];
                _contrastColors.RemoveAt(colorIndex);
            }
            else
            {
                color = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            }

            chip.GetComponent<SpriteRenderer>().color = color;
            Nodes[chip.finishNodeIndex].GetComponent<SpriteRenderer>().color = color;
        }
    }


    private void DrawConnections()
    {
        foreach (Connection connection in Connections)
        {
            DrawConnectionLine(connection);
        }
    }


    private void DrawConnectionLine(Connection connection)
    {
        LineRenderer lineRenderer = Instantiate(_linePrefab).GetComponent<LineRenderer>();
        Vector2 point1 = new Vector2(Nodes[connection.nodesIndexes[0]].transform.position.x, Nodes[connection.nodesIndexes[0]].transform.position.y);
        Vector2 point2 = new Vector2(Nodes[connection.nodesIndexes[1]].transform.position.x, Nodes[connection.nodesIndexes[1]].transform.position.y);
        lineRenderer.SetPosition(0, point1);
        lineRenderer.SetPosition(1, point2);
    }


    private void AddConnectedNodesInfoToNodes()
    {
        int node1Index;
        int node2Index;

        for (int i = 0; i < Connections.Count; i++)
        {
            node1Index = Connections[i].nodesIndexes[0];
            node2Index = Connections[i].nodesIndexes[1];

            Nodes[node1Index].connectedNodes.Add(Nodes[node2Index]);
            Nodes[node2Index].connectedNodes.Add(Nodes[node1Index]);
        }
    }


    private void HandleConnectionsNodesInfo()
    {
        for (int i = 0; i < _connectionsNumber; i++)
        {
            var valuesInLine = ReadNodesInfo();
            AddNodesInfoToConnection(i, valuesInLine);
        }
    }


    private void AddNodesInfoToConnection(int i, string[] valuesInLine)
    {
        Connections[i].nodesIndexes[0] = int.Parse(valuesInLine[0]) - 1;
        Connections[i].nodesIndexes[1] = int.Parse(valuesInLine[1]) - 1;
    }


    private string[] ReadNodesInfo()
    {
        var _valuesInLine = ReadValuesFromLine(_linesInText[_currentLine]);
        _currentLine++;
        return _valuesInLine;
    }


    private void CreateConnectionsList()
    {
        for (int i = 0; i < _connectionsNumber; i++)
        {
            Connections.Add(new Connection());
        }
    }


    private void ReadConnectionsNumber()
    {
        _connectionsNumber = int.Parse(_linesInText[_currentLine]);
        _currentLine++;
    }


    private void HandleChipsFinishNodesInfo()
    {
        var valuesInLine = ReadChipsFinishNodesInfo();
        AddFinishNodesInfoToChips(valuesInLine);
    }


    private void AddFinishNodesInfoToChips(string[] valuesInLine)
    {
        for (int i = 0; i < _chipsNumber; i++)
        {
            Chips[i].GetComponent<Chip>().finishNodeIndex = int.Parse(valuesInLine[i]) - 1;
        }
    }


    private string[] ReadChipsFinishNodesInfo()
    {
        var valuesInLine = ReadValuesFromLine(_linesInText[_currentLine]);
        _currentLine++;
        return valuesInLine;
    }


    private void HandleChipsStartNodesInfo()
    {
        var valuesInLine = ReadChipsStartNodesInfo();
        AddStartNodesInfoToChipsAndNodes(valuesInLine);
    }


    private void AddStartNodesInfoToChipsAndNodes(string[] valuesInLine)
    {
        for (int i = 0; i < _chipsNumber; i++)
        {
            int startNodeIndex = int.Parse(valuesInLine[i]) - 1;

            Chips[i].startNodeIndex = startNodeIndex;
            Chips[i].currentNodeIndex = startNodeIndex;

            Chips[i].transform.SetPositionAndRotation(Nodes[startNodeIndex].transform.position, Quaternion.identity);
            Nodes[startNodeIndex].chip = Chips[i];
        }
    }


    private string[] ReadChipsStartNodesInfo()
    {
        var valuesInLine = ReadValuesFromLine(_linesInText[_currentLine]);
        _currentLine++;
        return valuesInLine;
    }


    private void HandleNodesPositionsInfo()
    {
        for (int i = 0; i < _nodesNumber; i++)
        {
            var valuesInLine = ReadNodePositionInfo();
            AddPositionInfoToNode(i, valuesInLine);
        }
    }


    private void AddPositionInfoToNode(int i, string[] valuesInLine)
    {
        Vector2 position = new Vector2(float.Parse(valuesInLine[0]), float.Parse(valuesInLine[1]));
        Nodes[i].transform.SetPositionAndRotation(position, Quaternion.identity);
        Nodes[i].index = i;
    }


    private string[] ReadNodePositionInfo()
    {
        var _valuesInLine = ReadValuesFromLine(_linesInText[_currentLine]);
        _currentLine++;
        return _valuesInLine;
    }


    private void SpawnNodes()
    {
        for (int i = 0; i < _nodesNumber; i++)
        {
            Nodes.Add(Instantiate(_nodePrefab).GetComponent<Node>());
            Nodes[i].index = i;
        }
    }


    private void SpawnChips()
    {
        for (int i = 0; i < _chipsNumber; i++)
        {
            Chips.Add(Instantiate(_chipPrefab).GetComponent<Chip>());
            Chips[i].index = i;
        }
    }


    private void ReadNodesNumber()
    {
        _nodesNumber = int.Parse(_linesInText[_currentLine]);
        _currentLine++;
    }


    private void ReadChipsNumber()
    {
        _chipsNumber = int.Parse(_linesInText[_currentLine]);
        _currentLine++;
    }


    private void SplitTextIntoLines(string text)
    {
        _linesInText = text.Split(linesSeparator, StringSplitOptions.RemoveEmptyEntries);
    }


    private string[] ReadValuesFromLine(string text)
    {
        return text.Split(valuesSeparators, StringSplitOptions.RemoveEmptyEntries);
    }
}