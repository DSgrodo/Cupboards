using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DataInputFilesContainer : ScriptableObject
{
    [SerializeField] public List<TextAsset> DataInputFiles;
}