using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerPrefsKeysContainer : ScriptableObject
{
    public readonly string InputDataFileIndex = "InputDataFileIndex";
}
