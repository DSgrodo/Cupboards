using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SoundsNamesContainer : ScriptableObject
{
    public readonly string BackgroundMusic = "BackgroundMusic";
    public readonly string ButtonPressed = "ButtonPressed";
    public readonly string Click1 = "Click1";
    public readonly string Click2 = "Click2";
    public readonly string Win = "Win";
}
