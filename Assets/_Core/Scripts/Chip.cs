using UnityEngine;

public class Chip : MonoBehaviour
{
    [SerializeField] public GameObject highlighter;
    public int index;
    public int startNodeIndex;
    public int currentNodeIndex;
    public int finishNodeIndex;
}