using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] PlayerPrefsKeysContainer _playerPrefsKeys;

    public void LoadLevel(int inputDataFileIndex)
    {
        PlayerPrefs.SetInt(_playerPrefsKeys.InputDataFileIndex, inputDataFileIndex);
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
