using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    [SerializeField] private SoundsNamesContainer _sounds;

    public void PlaySound()
    {
        AudioManager.Instance.Play(_sounds.ButtonPressed);
    }
}
