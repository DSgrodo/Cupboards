using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    [SerializeField] public GameObject highlighter;
    public int index;
    public List<Node> connectedNodes = new List<Node>();
    public List<int> path;
    public Chip chip;
}