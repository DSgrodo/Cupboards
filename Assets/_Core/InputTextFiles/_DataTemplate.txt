ChipsNumber
6

NodesNumber
9

Nodes x y coordinates pairs (9)
100,100
200,100
300,100
100,200
200,200
300,200
100,300
200,300
300,300

Start chips positions (6)
1,2,3,7,8,9

Finish chips positions (6)
7,8,9,1,2,3

ConnectionsNumber
8

Nodes-to-connect pairs (8)
1,4
2,5
3,6
4,5
5,6
4,7
5,8
6,9